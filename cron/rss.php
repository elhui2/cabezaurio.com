<?php

include_once '../includes/config.php';
include_once 'SimpleImage.php';

$result = $mysql->query('SELECT * FROM sites');

while ($site = $result->fetch_assoc()) {

    try {
        $content = file_get_contents($site['rss_url']);
        $rssItems = new SimpleXmlElement($content);
    } catch (Exception $ex) {
        print 'Error al leer' . $site['rss_url'] . PHP_EOL;
    }

    foreach ($rssItems->channel->item as $item) {
        sleep(1);

        $exist = $mysql->query('SELECT * FROM rss_feed WHERE link="' . $item->link . '"');

        if ($exist->num_rows) {
            continue;
        }

        $imgItem = open_graph($item->link);

        if ($imgItem == NULL) {
            $imgItem = '';
        }



        if (!empty($item->pubDate)) {
            $pub_date = date('Y-m-d H:i:s', strtotime($item->pubDate));
        } else {
            $pub_date = NULL;
        }

        $title = $mysql->real_escape_string($item->title);
        //$title = utf8_encode($title);

        print $title . PHP_EOL;
        print $imgItem . PHP_EOL;
        print $item->link . PHP_EOL;

        $statement = $mysql->prepare('INSERT INTO rss_feed (id_sites,title,link,pub_date,og_image) VALUES (?,?,?,?,?)');
        $statement->bind_param('issss', $site['id'], $title, $item->link, $pub_date, $imgItem);

        if (!$statement->execute()) {
            print $mysql->error . PHP_EOL;
        }
    }
}

function open_graph($link) {
    $imgName = date('Ymdhis') . '.jpg';
    libxml_use_internal_errors(true);
    $c = file_get_contents($link);
    $d = new DomDocument();
    $d->loadHTML($c);
    $xp = new domxpath($d);
    foreach ($xp->query("//meta[@property='og:image']") as $el) {
        print $imgName . PHP_EOL;

        $ch = curl_init($el->getAttribute("content"));
        $fp = fopen('../images/' . $imgName, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        try {
            $img = new abeautifulsite\SimpleImage('../images/' . $imgName);
            $img->thumbnail(320, 180)->save('../thumbs/320_180_' . $imgName);
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return FALSE;
        }
        
        return '../thumbs/320_180_' . $imgName;
    }
}
