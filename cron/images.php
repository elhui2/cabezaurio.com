<?php

include_once '../includes/config.php';
include_once 'SimpleImage.php';

$result = $mysql->query('SELECT * FROM rss_feed');

while ($link = $result->fetch_assoc()) {
    
    $imgName = md5('Image' . $link['id']) . '.jpg';
    
    if (file_exists('../thumbs/320_180_' . $imgName)){
        continue;
    }

    $c = file_get_contents($link['link']);
    $d = new DomDocument();
    $d->loadHTML($c);
    $xp = new domxpath($d);

    foreach ($xp->query("//meta[@property='og:image']") as $el) {

        print $el->getAttribute("content") . PHP_EOL;
        $imgUrl = $el->getAttribute("content");

        if (empty($imgUrl)) {
            continue;
        }

        $ch = curl_init($el->getAttribute("content"));

        $fp = fopen('../images/' . $imgName, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        try {
            $img = new abeautifulsite\SimpleImage('../images/' . $imgName);
            $img->thumbnail(320, 180)->save('../thumbs/320_180_' . $imgName);
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            continue;
        }

        $mysql->query('UPDATE rss_feed SET og_image="' . '/thumbs/320_180_' . $imgName . '" WHERE id=' . $link['id']);
    }
}