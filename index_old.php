
<?php
include_once 'includes/config.php';

$result = $mysql->query('SELECT *,(SELECT name FROM sites WHERE sites.id=rss_feed.id_sites) as site_name FROM rss_feed ORDER BY pub_date DESC LIMIT 36');
?>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cabezaurio</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body>

        <nav class="top-bar" data-topbar>
            <ul class="title-area">

                <li class="name">
                    <h1>
                        <a href="/">
                            Cabezaurio <sup>Alpha</sup>
                        </a>
                    </h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>

        </nav>
        <?php
        while ($item = $result->fetch_assoc()) {
            ?>
            <div class="large-3 columns">
                <div class="panel">
                    <h6><a href="<?php echo $item['link'] ?>"><?php echo $item['title'] ?></a></h6>
                    <a href="<?php echo $item['link'] ?>" target="_blank"><img src="<?php echo $item['og_image'] ?>" alt="<?php echo $item['title'] ?>"></a>
                    <p><?php echo $item['site_name'] ?></p>
                </div>
            </div>
            <?php
        }
        ?>




        <div class="large-12 columns">
            <hr/>
            <div class="row">
                <div class="large-6 columns">
                    <p>&copy; Copyright no one at all. Go to town.</p>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="js/foundation.min.js"></script>
        <script src="js/app.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36488649-12', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>


