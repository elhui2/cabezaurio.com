<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $this->load->helper('url');
        $this->load->model('rss_feed');
        $this->load->library('pagination');

        $config = array();
        $config["base_url"] = base_url() . 'pagina/';
        $config["total_rows"] = $this->rss_feed->record_count();
        $config["per_page"] = 31;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '<<';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['last_link'] = '>>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '<';
        $config['cur_tag_open'] = '<li class="current"><a>';
        $config['cur_tag_close'] = '</li></a>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $dataView = array(
            'feed' => $this->rss_feed->sort($config["per_page"], $page),
            'links' => $this->pagination->create_links()
        );
        $this->load->view('home', $dataView);
    }

    public function hello_world() {
        $this->load->view('hello_world');
    }

}
