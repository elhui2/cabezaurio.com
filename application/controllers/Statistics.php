<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends CI_Controller {

    public function set_view() {
        if (!$this->input->post('link')) {
            return $this->ajax_response(403, FALSE, 'Parametros incompletos');
        }

        $this->load->model(array('views', 'rss_feed'));

        $link = $this->rss_feed->get_by_link($this->input->post('link'));

        if (!$link) {
            return $this->ajax_response(403, FALSE, 'Parametros Incorrectos');
        }

        $dataView = array(
            'id_sites' => $link['id_sites'],
            'id_rss_feed' => $link['id']
        );

        if ($this->views->create($dataView)) {
            return $this->ajax_response(200, TRUE, 'El registro se guardo con exito!');
        } else {
            return $this->ajax_response(500, FALSE, 'Ocurrio un error');
        }
    }

    /**
     * ajax_response
     * Todas las repuestas para ajax en json
     * @version 0.2.5 Terminus
     * @param int $code Codigo http de la transaccion
     * @param boolean $status Estatus de la operacion
     * @param string $message Mensaje de la transaccion
     * @param array $response Datos de la transaccion OPCIONAL
     * @return type
     */
    protected function ajax_response($code, $status, $message, $response = NULL) {

        $resTranza = array(
            'code' => $code,
            'status' => $status,
            'message' => $message,
            'response' => $response
        );

        error_log(json_encode($resTranza));

        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(json_encode($resTranza));
    }

}
