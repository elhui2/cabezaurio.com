<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sites extends CI_Controller {

    public function index() {
        $this->load->model('sites_model');
        $dataView = array(
            'sites' => $this->sites_model->get_all()
        );
        $this->load->view('sites', $dataView);
    }

    public function site() {
        if (!$this->uri->segment(2)) {
            redirect('/');
        }

        $this->load->model(array('sites_model', 'rss_feed'));

        $site = $this->sites_model->read($this->uri->segment(2));
        if (!$site) {
            redirect('/');
        }

        $rss_feed = $this->rss_feed->sort(24, 0, $this->uri->segment(2));

        $dataView = array(
            'site' => $site,
            'rss_feed' => $rss_feed
        );
        $this->load->view('site', $dataView);
    }

    public function chart_site() {
        if (!$this->input->get('id_site')) {
            return $this->ajax_response(404, FALSE, 'Not Found');
        }

        $this->load->model(array('sites_model', 'rss_feed'));
        
        $chart = $this->rss_feed->get_chart($this->input->get('id_site'));
        
        return $this->ajax_response(200, TRUE, 'Sitio con registros', $chart);
    }

    public function pagination() {
        $this->load->model(array('rss_feed'));

        $rss_feed = $this->rss_feed->sort(16, $this->input->get('start'), $this->input->get('id_sites'));
        if ($rss_feed) {
            return $this->ajax_response(200, TRUE, 'Si hay marchanta!', $rss_feed);
        } else {
            return $this->ajax_response(403, FALSE, 'Uy no, se la debo!');
        }
    }

    /**
     * ajax_response
     * Todas las repuestas para ajax en json
     * @version 0.3.5 Alpha
     * @param int $code Codigo http de la transaccion
     * @param boolean $status Estatus de la operacion
     * @param string $message Mensaje de la transaccion
     * @param array $response Datos de la transaccion OPCIONAL
     * @return type
     */
    protected function ajax_response($code, $status, $message, $response = NULL) {

        $resTranza = array(
            'code' => $code,
            'status' => $status,
            'message' => $message,
            'response' => $response
        );

//        error_log(json_encode($resTranza));

        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(json_encode($resTranza));
    }

}
