<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['pagina/(:num)'] = 'home';
$route['pagina'] = 'home';
$route['hola-mundo'] = 'home/hello_world';

$route['sitios'] = 'sites';
$route['sitio/(:num)'] = 'sites/site';
