<?php

/**
 * rss_feed
 * Modelo de la tabla rss_feed
 * @package Terminus 0.1.1
 * @version Version 0.1.1 Terminus
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');
/*
  Estructura de la tabla:

  CREATE TABLE `rss_feed` (
  `id` int(11) NOT NULL,
  `id_sites` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `og_image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `link` varchar(255) CHARACTER SET latin1 NOT NULL,
  `pub_date` datetime DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  ALTER TABLE `rss_feed`
  ADD PRIMARY KEY (`id`);
  ALTER TABLE `rss_feed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

 */

class Rss_feed extends CI_Model {

    /**
     * env
     * Caracteristicas importantes de la tabla
     * "name" => "Nombre de la Tabla"
     * @var array 
     */
    protected $env;

    /**
     * @version string
     */
    public function __construct() {
        parent::__construct();
        $this->env = array(
            'name' => 'rss_feed'
        );
        $this->load->helper('url');
    }

    /**
     * create
     * Inserta un registro en la tabla
     * @param array $dataRegister Array asociativo con los datos del registro
     * @return mixed array con el registro o false
     */
    public function create($dataRegister) {
        if ($this->db->insert($this->env['name'], $dataRegister)) {
            return TRUE;
        }
    }

    /**
     * read
     * Lee un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return mixed array con el registro o false
     */
    public function read($idRegister) {
        $this->db->where('id', $idRegister);
        $result = $this->db->get($this->env['name']);
        return $result->result_array()[0];
    }

    /**
     * update
     * @param array $dataRegister Array asociativo con los datos del registro
     * @param int $idRegister Llave primaria del registro en la tabla
     */
    public function update($dataRegister, $idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->update($this->env['name'], $dataRegister)) {
            return $this->read($idRegister);
        } else {
            return FALSE;
        }
    }

    /**
     * delete
     * Elimina un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return boolean
     */
    public function delete($idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->delete($this->env['name'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * record_count
     * Regresa los registros totales de la tabla
     * @return type
     */
    public function record_count() {
        return $this->db->count_all("rss_feed");
    }

    /**
     * sort
     * Paginador de registros
     * @version 0.3.5
     * @param type $limit
     * @param type $start
     * @return boolean
     */
    public function sort($limit, $start, $idSites = FALSE) {
        $this->db->select('*,(SELECT count(*) FROM views WHERE rss_feed.id=views.id_rss_feed) as views,DATE_FORMAT(pub_date,"%Y-%M-%e %H:%i") AS pimp_date,(SELECT name FROM sites WHERE rss_feed.id_sites=sites.id) AS site_name');
        if ($idSites) {
            $this->db->where('id_sites', $idSites);
        }
        $this->db->order_by('pub_date', 'desc');

        $query = $this->db->get("rss_feed", $limit, $start);
//        error_log($this->db->last_query());
        if ($query->num_rows() == 0) {
            return false;
        }

        return $query->result_array();
    }

    public function get_by_link($link) {

        $this->db->where('link', $link);

        $result = $this->db->get('rss_feed');

        if ($result->num_rows()) {
            return $result->result_array()[0];
        } else {
            return FALSE;
        }
    }

    public function get_chart($id_site, $year_month = FALSE) {
        $this->db->select('COUNT(*) AS publications, DAY(pub_date) AS day');
        $this->db->from('rss_feed');
        $this->db->where('id_sites', $id_site);
        $this->db->where('DATE_FORMAT(pub_date,"%Y%m")', date('Ym'));
        $this->db->group_by('day');
        $result = $this->db->get();
        error_log($this->db->last_query());
        if (!$result->num_rows()) {
            return FALSE;
        }
        
        $publications = $result->result_array();
        $response = array();
        foreach ($publications as $daypub) {
            $response['categories'][] = $daypub['day'];
            $response['data'][] = (int) $daypub['publications'];
        }

        return $response;
    }

}
