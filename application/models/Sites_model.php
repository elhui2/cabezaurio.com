<?php

/**
 * sites
 * Modelo de la tabla sites
 * @package Terminus 0.1.1
 * @version Version 0.1.1 Terminus
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');
/*
  Estructura de la tabla:

  CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `rss_url` varchar(255) CHARACTER SET latin1 NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`);
  ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

 */

class Sites_model extends CI_Model {

    /**
     * env
     * Caracteristicas importantes de la tabla
     * "name" => "Nombre de la Tabla"
     * @var array 
     */
    protected $env;

    /**
     * @version string
     */
    public function __construct() {
        parent::__construct();
        $this->env = array(
            'name' => 'sites'
        );
    }

    /**
     * create
     * Inserta un registro en la tabla
     * @param array $dataRegister Array asociativo con los datos del registro
     * @return mixed array con el registro o false
     */
    public function create($dataRegister) {
        if ($this->db->insert($this->env['name'], $dataRegister)) {
            return TRUE;
        }
    }

    /**
     * read
     * Lee un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return mixed array con el registro o false
     */
    public function read($idRegister) {
        $this->db->where('id', $idRegister);
        $result = $this->db->get($this->env['name']);
        return $result->result_array()[0];
    }

    /**
     * update
     * @param array $dataRegister Array asociativo con los datos del registro
     * @param int $idRegister Llave primaria del registro en la tabla
     */
    public function update($dataRegister, $idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->update($this->env['name'], $dataRegister)) {
            return $this->read($idRegister);
        } else {
            return FALSE;
        }
    }

    /**
     * delete
     * Elimina un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return boolean
     */
    public function delete($idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->delete($this->env['name'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * record_count
     * Regresa los registros totales de la tabla
     * @return type
     */
    public function record_count() {
        return $this->db->count_all($this->env['name']);
    }

    /**
     * sort
     * Paginador de la tabla
     * @param type $limit
     * @param type $start
     * @return boolean
     */
    public function sort($limit, $start) {
        $query = $this->db->get($this->env['name'], $limit, $start);

        if ($query->num_rows() == 0) {
            return false;
        }

        return $query->result_array();
    }

    public function get_all() {
        $this->db->select('id,name,(SELECT COUNT(*) FROM rss_feed WHERE sites.id=rss_feed.id_sites ) AS links');
        $this->db->order_by('name');
        $result = $this->db->get('sites');
        if ($result->num_rows()) {
            return $result->result_array();
        } else {
            return FALSE;
        }
    }

}
