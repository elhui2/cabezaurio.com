<?php

/**
 * views
 * Modelo de la tabla views
 * @package Terminus 0.1.1
 * @version Version 0.1.1 Terminus
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');
/*
  Estructura de la tabla:

  CREATE TABLE IF NOT EXISTS `views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sites` int(11) NOT NULL,
  `id_rss_feed` int(11) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

 */

class Views extends CI_Model {

    /**
     * env
     * Caracteristicas importantes de la tabla
     * "name" => "Nombre de la Tabla"
     * @var array 
     */
    protected $env;

    /**
     * @version string
     */
    public function __construct() {
        parent::__construct();
        $this->env = array(
            'name' => 'views'
        );
    }

    /**
     * create
     * Inserta un registro en la tabla
     * @param array $dataRegister Array asociativo con los datos del registro
     * @return mixed array con el registro o false
     */
    public function create($dataRegister) {
        if ($this->db->insert($this->env['name'], $dataRegister)) {
            return TRUE;
        }
    }

    /**
     * read
     * Lee un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return mixed array con el registro o false
     */
    public function read($idRegister) {
        $this->db->where('id', $idRegister);
        $result = $this->db->get($this->env['name']);
        return $result->result_array()[0];
    }

    /**
     * update
     * @param array $dataRegister Array asociativo con los datos del registro
     * @param int $idRegister Llave primaria del registro en la tabla
     */
    public function update($dataRegister, $idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->update($this->env['name'], $dataRegister)) {
            return $this->read($idRegister);
        } else {
            return FALSE;
        }
    }

    /**
     * delete
     * Elimina un registro de la tabla
     * @param type $idRegister Llave primaria del registro en la tabla
     * @return boolean
     */
    public function delete($idRegister) {
        $this->db->where('id', $idRegister);
        if ($this->db->delete($this->env['name'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * record_count
     * Regresa los registros totales de la tabla
     * @return type
     */
    public function record_count() {
        return $this->db->count_all($this->env['name']);
    }

    /**
     * sort
     * Paginador de la tabla
     * @param type $limit
     * @param type $start
     * @return boolean
     */
    public function sort($limit, $start) {
        $query = $this->db->get($this->env['name'], $limit, $start);

        if ($query->num_rows() == 0) {
            return false;
        }

        return $query->result_array();
    }

}
