<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sitios</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
    </head>
    <body>

        <?php
        $this->load->view('layout/menu');
        ?>
        <div class="large-12 columns" id="feed">
            <?php
            foreach ($sites as $item) {
                ?>
                <div class="large-3 column item">
                    <p class="sitename"><a href="sitio/<?php echo $item['id'] ?>"><i class="fi-web"></i> <?php echo $item['name'] ?></a></p>
                    <p class="lead">Enlaces: <?php echo $item['links'] ?></p>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="large-12">
            <hr/>
            <div class="large-10 large-offset-1">
                <p><a href="http://elhui2.info">&copy; Copyleft elhui2 2016</a> Proyecto en GIT por <a href="https://bitbucket.org/elhui2/cabezaurio.com">Bitbucket</a></p>
            </div>

        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">

        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href='/assets/css/app.css' rel='stylesheet' type='text/css'>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

        <script src="/assets/js/foundation.min.js"></script>
        <script src="/assets/js/statistics.js"></script>
        <script src="/assets/js/app.js"></script>
    </body>
</html>


