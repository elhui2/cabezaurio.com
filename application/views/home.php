<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cabezaurio</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=1106568776064452";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <?php
        $this->load->view('layout/menu');

        if (isset($links)) {
            ?>
            <div class="large-12">
                <div class="pagination-centered">
                    <?php echo $links; ?>
                </div>
            </div>
            <?php
        }
        ?>


        <div class="large-12 columns" id="feed">
            <div class="large-3 column item">
                <div class="fb-like" data-href="http://cabezaurio.com" data-width="300" data-layout="box_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
            </div>
            <?php
            foreach ($feed as $item) {
                $imgUrl = $item['og_image'];
                if (empty($item['og_image'])) {
                    $imgUrl = '/assets/ui/placeholder.png';
                }
                ?>
                <div class="large-3 column item">
                    <p class="sitename"><a href="/sitio/<?php echo $item['id_sites'] ?>"><i class="fi-web"></i> <?php echo $item['site_name'] ?></a></p>
                    <a href="<?php echo $item['link'] ?>" target="_blank" class="ad-view">
                        <img src="<?php echo $imgUrl ?>" alt="<?php echo $item['title'] ?>" onerror="this.src='/assets/ui/placeholder.png';">
                    </a>
                    <span class="secondary label"><?php echo $item['pimp_date'] ?></span>
                    <span class="label"><i class="fi-eye"></i> <?php echo $item['views'] ?></span>
                    <a href="<?php echo $item['link'] ?>" target="_blank" class="title ad-view"><?php echo $item['title'] ?></a>
                </div>
                <?php
            }
            ?>
        </div>


        <?php
        if (isset($links)) {
            ?>
            <div class="large-12">
                <div class="pagination-centered">
                    <?php echo $links; ?>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="large-12">
            <hr/>
            <div class="large-10 large-offset-1">
                <p><a href="http://elhui2.info">&copy; Copyleft elhui2 2016</a> Proyecto en GIT por <a href="https://bitbucket.org/elhui2/cabezaurio.com">Bitbucket</a></p>
            </div>

        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">

        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href='/assets/css/app.css' rel='stylesheet' type='text/css'>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

        <script src="/assets/js/foundation.min.js"></script>
        <script src="/assets/js/statistics.js"></script>
        <script src="/assets/js/app.js"></script>
    </body>
</html>


