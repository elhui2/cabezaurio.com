<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $site['Name'] ?></title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
    </head>
    <body>
        <input type="hidden" name="id_site" id="id_site" value="<?php echo $site['id'] ?>">

        <?php
        $this->load->view('layout/menu');
        ?>
        <div class="large-12 columns" id="feed">
            <h1 class="text-center"><?php echo $site['Name'] ?></h1>
            <div id="chart-publications"></div>
            <?php
            foreach ($rss_feed as $item) {
                $imgUrl = $item['og_image'];
                if (empty($item['og_image'])) {
                    $imgUrl = '/assets/ui/placeholder.png';
                }
                ?>
                <div class="large-3 column item">
                    <a href="<?php echo $item['link'] ?>" target="_blank" class="ad-view">
                        <img src="<?php echo $imgUrl ?>" alt="<?php echo $item['title'] ?>" onerror="this.src='/assets/ui/placeholder.png';">
                    </a>
                    <span class="secondary label"><?php echo $item['pimp_date'] ?></span>
                    <span class="label"><i class="fi-eye"></i> <?php echo $item['views'] ?></span>
                    <a href="<?php echo $item['link'] ?>" target="_blank" class="title ad-view"><?php echo $item['title'] ?></a>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="large-12 text-center">
            <button class="button primary" id="load-more">Cargar Más</button>
        </div>
        <div class="large-12">
            <hr/>
            <div class="large-10 large-offset-1">
                <p><a href="http://elhui2.info">&copy; Copyleft elhui2 2016</a> Proyecto en GIT por <a href="https://bitbucket.org/elhui2/cabezaurio.com">Bitbucket</a></p>
            </div>

        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">

        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href='/assets/css/app.css' rel='stylesheet' type='text/css'>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="/assets/js/foundation.min.js"></script>
        <script src="/assets/js/statistics.js"></script>
        <script src="/assets/js/app.js"></script>
        <script src="/assets/js/site.js"></script>
        <script type="text/javascript">
            
            
        </script>
    </body>
</html>


