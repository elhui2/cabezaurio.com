<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hola Mundo!</title>

    </head>
    <body>


        <?php
        $this->load->view('layout/menu');

        if (isset($links)) {
            ?>
            <div class="large-12">
                <div class="pagination-centered">
                    <?php echo $links; ?>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="large-6 large-offset-3 columns">

            <img src="/assets/ui/logo.jpg" style="display: block; max-width: 320px; margin: 0 auto" class="img-responsive">
            <h1>Hola Mundo!</h1>
            <p>Cabezaurio solo es un experimento, un estudio para conocer internet, solo un esqueleto. ¡La pura calavera!</p>
            <p>Si de algún modo el contenido viola alguna ley, o es inapropiado no es bronca de Cabezaurio solo es un pequeño lector de noticias externas y no sabe nada acerca de leyes nacionales o internacionales, el contenido será removido de inmediato, ¡Cabezaurio no busca ser transgresivo!, si eres administrador del sitio y quieres quitar tus contenidos, con gusto los manda a la mierda. Si quieres agregar un canal, Mandale correo a contacto@cabezaurio.com y que buen p2 :P</p>

            <blockquote class="text-center">
                Qué tal si deliramos por un ratito<br>
                qué tal si clavamos los ojos más allá de la infamia<br>
                para  adivinar otro mundo posible<br>
                En las calles los automóviles serán aplastados por los perros<br>
                la gente no será manejada por el automóvil<br>
                ni será programada por el ordenador<br>
                ni será comprada por el supermercado<br>
                ni será tampoco mirada por el televisor.<br>
                <cite>Eduardo Galeano. Derecho al Delirio</cite>
            </blockquote>

            <p>La idea es instalar un cron, que lea los canales <a href="http://cyber.harvard.edu/rss/rss.html" title="RSS 2.0">RSS 2.0</a> de algunos sitios cada hora y estudiar el comportamiento, <i>ver que pasa.</i></p>

            <p>La primer pregunta que se le ocurre: ¿Quien debería tener más trafico?, ¿El que publica la noticia primero o el que la sirve mejor?, Accesibilidad, optimización, diseño, bla bla bla….</p>

            <p>¿Por que saber eso?<br>Si un artista sube una obra, merece estar en los mejores sitios, y los internautas queremos ver un contenido de calidad y de forma eficiente.</p>


            <p>Cabezaurio quiere ser transparente como el viento, es Open Source, Creative Commons. Cabezaurio cree que internet es una base infinita de conocimiento colectivo <strong>La Utopía</strong> y quiere aprender de ella.</p>

            <blockquote>
                <p>Ojalá podamos merecer que nos llamen locos, como han sido llamadas locas las Madres de Plaza de Mayo, por cometer la locura de negarnos a olvidar en los tiempos de la amnesia obligatoria.</p>
                <p>Ojalá podamos ser capaces de seguir caminando los caminos del viento, a pesar de las caídas y las traiciones y las derrotas, porque la historia continúa, más allá de nosotros, y cuando ella dice adiós, está diciendo: hasta luego.</p>
                <p>Ojalá podamos mantener viva la certeza de que es posible ser compatriota y contemporáneo de todo aquel que viva animado por la voluntad de justicia y la voluntad de belleza, nazca donde nazca y viva cuando viva, porque no tienen fronteras los mapas del alma ni del tiempo.</p>
                <cite>Eduardo Galeano. Los caminos del viento</cite>
            </blockquote>
            <p>Solo le va a dar el tiempo libre, hay que sacar pa' la papa, camara mijos ahí nos topamos!</p>
        </div>


        <?php
        if (isset($links)) {
            ?>
            <div class="large-12">
                <div class="pagination-centered">
                    <?php echo $links; ?>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="large-12">
            <hr/>
            <div class="large-10 large-offset-1">
                <p><a href="http://elhui2.info">&copy; Copyleft elhui2 2016</a> Proyecto en GIT por <a href="https://bitbucket.org/elhui2/cabezaurio.com">Bitbucket</a></p>
            </div>

        </div>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <script type="text/javascript">
            //Google Analytics
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36488649-12', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>


