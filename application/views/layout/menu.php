<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="top-bar" data-topbar>
    <ul class="title-area">

        <li class="name">
            <h1>
                <a href="/">
                    Cabezaurio <sup>Alpha</sup>
                </a>
            </h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
    </ul>
    <section class="top-bar-section">
        <ul class="left">
            <li><a href="/sitios">Sitios</a></li>
            <li><a href="/hola-mundo">Hola mundo!</a></li>
        </ul>
    </section>
</nav>