/**
 * site
 * script de la vista site
 * @version 0.3.7
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
var pagination = 24;
var idSite = $('#id_site').val();

$(document).ready(function () {
    //Cargar mas notas
    $('#load-more').click(function () {
        $.getJSON('/sites/pagination', {start: pagination, id_sites: idSite}, function (response) {
            if (response.status) {
                var items = response.response;
                for (var i = 0; i < items.length; i++) {
                    console.debug(items[i].title);
                    var html = '<div class="large-3 column item">';
                    html += '<a href="' + items[i].link + '" target="_blank" class="ad-view">';
                    html += '<img src="' + items[i].og_image + '" alt="' + items[i].title + '" onerror="this.src=\'/assets/ui/placeholder.png\';">';
                    html += '</a>';
                    html += '<span class="secondary label">' + items[i].pimp_date + '</span>';
                    html += '<span class="label"><i class="fi-eye"></i> ' + items[i].views + '</span>';
                    html += '<a href="' + items[i].link + '" target="_blank" class="title ad-view">' + items[i].title + '</a>';
                    html += '</div>';
                    $('#feed').append(html);
                }
                pagination += 16;
            } else {
                $('#load-more').remove();
            }
        });
        //Aplica hurracarrana y el listener del click de nuez
        setTimeout(function () {
            loadDom();
        }, 1500);

    });
    $.getJSON('/sites/chart_site', {id_site: idSite}, function (response) {
        if (response.status) {
            Highcharts.chart('chart-publications', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Publicaciones de este mes'
                },
                subtitle: {
                    text: 'Canal RSS 2.0 del sitio'
                },
                xAxis: {
                    categories: response.response.categories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Numero de publicaciones'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">Día {point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">Notas: </td>' +
                            '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'Día',
                        data: response.response.data
                    }]
            });
        }
        console.debug(response);
    });
});
