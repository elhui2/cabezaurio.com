/**
 * app.js
 * script general del sitio
 * @version 0.3.7
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */

//Google Analytics
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-36488649-12', 'auto');
ga('send', 'pageview');

//PIWIK
var _paq = _paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function () {
    var u = "//cabezaurio.com/estadisticas/";
    _paq.push(['setTrackerUrl', u + 'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript';
    g.async = true;
    g.defer = true;
    g.src = u + 'piwik.js';
    s.parentNode.insertBefore(g, s);
})();

//Cargar acciones predeterminadas
function loadDom() {
    $('.item .ad-view').click(function () {
        //Url
        var link = $(this).attr('href');
        
        //Track visita piwik
        _paq.push(['trackEvent', 'visita', link]);
        
        //@deprecated
        //Mi track, obviamente obsoleto lo dejo un rato solo por morbo xD
        setView(link);
        setTimeout(function () {
            window.open(link);
        }, 150);
        //@deprecated
        
        return false;
    });
}

//Foundation
$(document).foundation();

//Jquery
$(document).ready(function () {
    loadDom();
});

